package net.downwithdestruction.dwdchat.channel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.downwithdestruction.dwdchat.DwDChat;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class ChannelCtrl {
	private final static DwDChat plugin = DwDChat.getInstance();
	
	private static ConfigurationSection channelConfSec = plugin.getConfig().getConfigurationSection("channels");
	private static final List<Channel> channels = new ArrayList<Channel>();
	
	public static void addAllChannels() {
		for (String key : channelConfSec.getKeys(false)) {
			ConfigurationSection cs = channelConfSec.getConfigurationSection(key);
			Channel c = new Channel(cs);
			synchronized (channels) {
				channels.add(c);
			}
		}
	}
	
	public static void reloadAllChannels() {
		Map<String, List<String>> members = new HashMap<String, List<String>>();
		synchronized (channels) {
			for (Channel c : channels) members.put(c.getName(), c.getMembers());
		}
		removeAllChannels();
		addAllChannels();
		synchronized (channels) {
			for (Channel c : channels) {
				if (members.get(c.getName()) == null) continue;
				for (String s : members.get(c.getName()))
					c.addMember(plugin.getServer().getOfflinePlayer(s));
			}
		}
	}
	
	public static void removeAllChannels() {
		synchronized (channels) {
			channels.clear();
		}
	}
	
	public static int addNewChannels() {
		reload();
		int added = 0;
		for (String key : channelConfSec.getKeys(false)) {
			ConfigurationSection cs = channelConfSec.getConfigurationSection(key);
			if (getChannel(key) != null) continue;
			Channel c = new Channel(cs);
			synchronized (channels) {
				channels.add(c);
			}
			added++;
		}
		return added;
	}
	
	public static void reload() {
		channelConfSec = plugin.getConfig().getConfigurationSection("channels");
	}
	
	public static Channel getChannel(String name) {
		for (Channel c : channels) {
			synchronized (channels) {
				if (c.getName().equalsIgnoreCase(name)) return c;
			}
		}
		return null;
	}
	
	public static Channel getPlayerChannel(Player p) {
		for (Channel c : channels) {
			synchronized (channels) {
				if (c.isMember(p)) return c;
			}
		}
		return null;
	}
	
	@Deprecated
	public static boolean isInAChannel(Player p) {
		for (Channel c : channels) {
			synchronized (channels) {
				if (c.isMember(p)) return true;
			}
		}
		return false;
	}
	
	public static boolean addToDefaultChannel(Player p) {
		for (Channel c : channels) {
			synchronized (channels) {
				if (c.getIsDefault()) {
					c.addMember(p);
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean addChannel(Channel c) throws IllegalArgumentException {
		if (c == null) throw new IllegalArgumentException("Channel cannot be null!");
		synchronized (channels) {
			if (channels.contains(c)) return false;
			channels.add(c);
		}
		return true;
	}
	
	public static boolean removeChannel(Channel c) throws IllegalArgumentException {
		if (c == null) throw new IllegalArgumentException("Channel cannot be null!");
		synchronized (channels) {
			if (!channels.contains(c)) return false;
			channels.remove(c);
		}
		return true;
	}
}
