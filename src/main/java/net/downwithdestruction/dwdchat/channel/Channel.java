package net.downwithdestruction.dwdchat.channel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.downwithdestruction.dwdchat.DwDChat;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class Channel {
    private final static DwDChat plugin = DwDChat.getInstance();
    private final static String configFile = plugin.getDataFolder() + File.separator + "config.yml";
    private final String name;
    private String chatAdmin;
    private Double chatRadius;
    private Boolean colorAllowed;
    private final String chatFormat;
    private final Boolean isDefault;
    private final Boolean alwaysSeen;
    private final ConfigurationSection config;
    private final List<String> members = new ArrayList<String>();
    public Channel(ConfigurationSection values) {
        name = values.getString("name", "Error Channel");
        chatAdmin = values.getString("admin", "OP"); // default to "OP" if no admin is found. All opers will be admin to this if thats the case.
        chatFormat = values.getString("chat-format", "[err] {name}: {message}");
        chatRadius = values.getDouble("chat-radius", 0);
        isDefault = values.getBoolean("default", false);
        alwaysSeen = values.getBoolean("always-seen", false);
        colorAllowed = values.getBoolean("color-allowed", true);
        config = values;
    }
    public String getName() {
        return name;
    }
    public String getAdmin() {
        return chatAdmin;
    }
    public String getChatFormat() {
        return chatFormat;
    }
    public double getChatRadius() {
        return chatRadius;
    }
    public boolean getIsDefault() {
        return isDefault;
    }
    public boolean getAlwaysSeen() {
        return alwaysSeen;
    }
    public boolean getColorAllowed() {
        return colorAllowed;
    }
    public List<String> getMembers() {
        return members;
    }
    public List<Player> getPlayers() {
        List<Player> players = new ArrayList<Player>();
        for (String s : members) {
            Player p = plugin.getServer().getPlayer(s);
            if (s == null) {
                continue;
            }
            players.add(p);
        }
        return players;
    }
    public boolean isMember(Player p) {
        if (p == null) {
            return false;
        }
        synchronized (members) {
            return members.contains(p.getName());
        }
    }
    public boolean isAdmin(Player p) {
        if (p == null) {
            return false;
        }
        synchronized (chatAdmin) {
            if (chatAdmin.equals("OP")) {
                return p.isOp();
            }
            return chatAdmin.equals(p.getName());
        }
    }
    public void addMember(OfflinePlayer p) {
        if (p == null) {
            return;
        }
        synchronized (members) {
            members.add(p.getName());
        }
    }
    public void removeMember(Player p) {
        if (p == null) {
            return;
        }
        synchronized (members) {
            if (members.contains(p.getName())) {
                members.remove(p.getName());
            }
        }
    }
    public void sendMessage(String message) {
        if (message == null) {
            return;
        }
        for (String s : members) {
            Player p = plugin.getServer().getPlayer(s);
            if (p == null) {
                continue;
            }
            p.sendMessage(message);
        }
    }
    public void sendMessage(String message, List<Player> recipients) {
        for (Player p : recipients) {
            if (p == null) {
                continue;
            }
            if (!isMember(p)) {
                continue;
            }
            p.sendMessage(message);
        }
    }
    public boolean setAdmin(String name) {
        if (!plugin.getServer().getOfflinePlayer(name).hasPlayedBefore()) {
            return false;
        }
        synchronized (chatAdmin) {
            String oldAdmin = chatAdmin;
            try {
                config.set("admin", name);
                plugin.getConfig().save(configFile);
            } catch (IOException e) {
                config.set("admin", oldAdmin); // Fix the live config (in memory)
                return false;
            }
            chatAdmin = name;
        }
        return true;
    }
    public boolean setColorAllowed(boolean allow) {
        boolean oldAllow = colorAllowed;
        try {
            config.set("color-allowed", allow);
            plugin.getConfig().save(configFile);
        } catch (IOException e) {
            config.set("color-allowed", oldAllow); // Fix the live config (in memory)
            return false;
        }
        colorAllowed = allow;
        return true;
    }
    public boolean setRadius(Double radius) {
        if (radius < 0) {
            return false;
        }
        synchronized (chatRadius) {
            Double oldRadius = chatRadius;
            try {
                config.set("radius", radius);
                plugin.getConfig().save(configFile);
            } catch (IOException e) {
                config.set("radius", oldRadius); // Fix the live config (in memory)
                return false;
            }
            chatRadius = radius;
        }
        return true;
    }
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Channel)) {
            return false;
        }
        Channel c = (Channel) o;
        return c.getName().equals(name);
    }
}