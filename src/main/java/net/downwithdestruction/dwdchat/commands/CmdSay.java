package net.downwithdestruction.dwdchat.commands;

import net.downwithdestruction.dwdchat.DwDChat;
import net.downwithdestruction.dwdchat.utils.Utils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdSay implements CommandExecutor {
	private final static DwDChat plugin = DwDChat.getInstance();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("say")) {
			if (!Utils.isAuthorized(cs, "dwdchat.say")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			String message = Utils.formatText(Utils.join(args, 0), cs, DwDChat.sayFormat);
			plugin.getServer().broadcastMessage(message);
			return true;
		}
		return false;
	}
}
