package net.downwithdestruction.dwdchat.commands;

import java.util.Date;

import net.downwithdestruction.dwdchat.DwDChat;
import net.downwithdestruction.dwdchat.utils.Utils;
import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.AFKUtils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdMe implements CommandExecutor {
	private final static DwDChat plugin = DwDChat.getInstance();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("me")) {
			if (!Utils.isAuthorized(cs, "dwdchat.me")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (DwDChat.getInstance().isVanished((Player) cs)) {
				cs.sendMessage(ChatColor.DARK_AQUA + "You cannot chat while vanished.");
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			if (DwDChat.getInstance().hasDwDCommands) {
				if (cs instanceof Player) {
					AFKUtils.setLastMove((Player) cs, (new Date()).getTime());
					if (AFKUtils.isAfk((Player) cs)) {
						AFKUtils.unsetAfk((Player) cs);
						DwDCommands.getInstance().getServer().broadcastMessage(ChatColor.GRAY + ((Player) cs).getDisplayName() + " is no longer AFK.");
					}
				}
			}
			String message = Utils.formatText(Utils.join(args, 0), (Player) cs, DwDChat.meFormat);
			plugin.getServer().broadcastMessage(message);
			return true;
		}
		return false;
	}
}
