package net.downwithdestruction.dwdchat.commands;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.downwithdestruction.dwdchat.DwDChat;
import net.downwithdestruction.dwdchat.channel.Channel;
import net.downwithdestruction.dwdchat.channel.ChannelCtrl;
import net.downwithdestruction.dwdchat.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdChannel implements CommandExecutor {
	private DwDChat plugin;
	
	public CmdChannel(DwDChat plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("channel")) {
			if (!Utils.isAuthorized(cs, "dwdchat.channel")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			if (args.length < 1) {
				if (cs instanceof Player) {
					Channel c = ChannelCtrl.getPlayerChannel((Player) cs);
					cs.sendMessage((c == null) ? ChatColor.BLUE + "You are currently not in a channel." : ChatColor.BLUE + "You are currently in " + ChatColor.GRAY + c.getName() + ChatColor.BLUE + ".");
					return true;
				}
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			Player p = (Player) cs;
			if (args[0].equalsIgnoreCase("set")) {
				Channel c = ChannelCtrl.getChannel(args[1]);
				if (c == null) {
					cs.sendMessage(ChatColor.RED + "No such channel!");
					return true;
				}
				if (!c.isAdmin(p)) {
					cs.sendMessage(ChatColor.RED + "You do not have permissions to this channel.");
					return true;
				}
				if (args.length < 4) {
					cs.sendMessage(ChatColor.RED + "Not enough parameters supplied!");
					return true;
				}
				if (args[2].equalsIgnoreCase("color-allow")) {
					if (args[3].equals("") || args[3] == null) {
						cs.sendMessage(ChatColor.RED + "You did not specify true or false.");
						cs.sendMessage(ChatColor.RESET + "/channel set [channel] color-allow [true/false]");
						return true;
					}
					if (c.setColorAllowed(args[3].equalsIgnoreCase("true") ? true : false)) {
						cs.sendMessage(ChatColor.LIGHT_PURPLE + "Colors for channel are now " + (args[3].equalsIgnoreCase("true") ? "enabled" : "disabled") + ".");
						return true;
					}
					cs.sendMessage(ChatColor.RED + "An error has occured. Color settings not changed.");
					return true;
				} else if (args[2].equalsIgnoreCase("radius")) {
					if (args[3].equals("") || args[3] == null) {
						cs.sendMessage(ChatColor.RED + "You did not specify a radius.");
						cs.sendMessage(ChatColor.RESET + "/channel set [channel] radius [radius]");
						return true;
					}
					Double radius;
					try {
						radius = Double.valueOf(args[3]);
					} catch (NumberFormatException e) {
						cs.sendMessage(ChatColor.RED + "Radius must be a valid number.");
						return true;
					}
					if (radius < 0) {
						cs.sendMessage(ChatColor.RED + "Radius must be 0 or higher.");
						return true;
					}
					if (c.setRadius(radius)) {
						cs.sendMessage(ChatColor.LIGHT_PURPLE + "Channel radius is now " + args[3] + ".");
						return true;
					}
					cs.sendMessage(ChatColor.RED + "An error has occured. Radius settings not changed.");
					return true;
				} else if (args[2].equalsIgnoreCase("admin")) {
					if (args[3].equals("") || args[3] == null) {
						cs.sendMessage(ChatColor.RED + "You did not specify a username.");
						cs.sendMessage(ChatColor.RESET + "/channel set [channel] admin [username]");
						return true;
					}
					if (c.setAdmin(args[3])) {
						cs.sendMessage(ChatColor.LIGHT_PURPLE + "New channel admin has been set.");
						return true;
					}
					cs.sendMessage(ChatColor.RED + "An error has occured. New admin not set.");
					return true;
				}
				cs.sendMessage(ChatColor.RED + "Parameter not recognized as a command.");
				return false;
			}
			if (args[0].equalsIgnoreCase("mute")) {
				Channel c = ChannelCtrl.getChannel(args[1]);
				if (c == null) {
					cs.sendMessage(ChatColor.RED + "No such channel!");
					return true;
				}
				if (!c.isAdmin(p)) {
					cs.sendMessage(ChatColor.RED + "You do not have permissions to this channel.");
					return true;
				}
				if (args.length < 3) {
					cs.sendMessage(ChatColor.RED + "Not enough parameters supplied!");
					return true;
				}
				if (args[2].equalsIgnoreCase("list")) {
					final Map<String, Object> opts = plugin.mutes.getValues(false);
					if (opts.keySet().isEmpty()) {
						cs.sendMessage(ChatColor.LIGHT_PURPLE + "No users are muted!");
						return true;
					}
					final List<String> mutedNames = new ArrayList<String>();
					for (String name : opts.keySet()) {
						if (plugin.mutes.getLong(name + "." + c.getName() + ".muted", 0) > 0) {
							mutedNames.add(name);
						}
					}
					if (mutedNames.isEmpty()) {
						cs.sendMessage(ChatColor.LIGHT_PURPLE + "No users are muted!");
						return true;
					}
					cs.sendMessage(ChatColor.LIGHT_PURPLE + "Muted users: " + mutedNames.toString());
					return true;
				}
				if (args.length < 4) {
					cs.sendMessage(ChatColor.RED + "Not enough parameters supplied!");
					return true;
				}
				if (args[2].equalsIgnoreCase("add")) {
					if (args[3].equals("") || args[3] == null) {
						cs.sendMessage(ChatColor.RED + "You did not specify a username.");
						cs.sendMessage(ChatColor.RESET + "/channel mute [channel] add [username] (seconds)");
						return true;
					}
					final String username = args[3];
					if (plugin.getServer().getPlayer(username) == null) {
						cs.sendMessage(ChatColor.RED + "That user is not currently online.");
						return true;
					}
					Integer seconds = 0;
					if (args.length > 4) {
						try {
							seconds = Integer.valueOf(args[4]);
						} catch (NumberFormatException e) {
							cs.sendMessage(ChatColor.RED + "Seconds must be a valid number.");
							return true;
						}
						if (seconds <= 0) {
							cs.sendMessage(ChatColor.RED + "Seconds must be higher than 0.");
							return true;
						}
					}
					try {
						plugin.mutes.set(username + "." + c.getName() + ".muted", new Date().getTime());
						plugin.mutes.set(username + "." + c.getName() + ".length", seconds);
						plugin.mutes.save(plugin.mutesFile);
					} catch (IOException e) {
						cs.sendMessage(ChatColor.RED + "An error has occured. User was NOT muted.");
						return true;
					}
					cs.sendMessage(ChatColor.RED + "User is now muted.");
					return true;
				} else if (args[2].equalsIgnoreCase("del")) {
					if (args[3].equals("") || args[3] == null) {
						cs.sendMessage(ChatColor.RED + "You did not specify a username.");
						cs.sendMessage(ChatColor.RESET + "/channel mute [channel] del [username]");
						return true;
					}
					final String username = args[3];
					if (plugin.getServer().getPlayer(username) == null) {
						cs.sendMessage(ChatColor.RED + "That user is not currently online.");
						return true;
					}
					try {
						plugin.mutes.set(username + "." + c.getName(), null);
						plugin.mutes.save(plugin.mutesFile);
					} catch (IOException e) {
						cs.sendMessage(ChatColor.RED + "An error has occured. User was NOT un-muted.");
						return true;
					}
					cs.sendMessage(ChatColor.RED + "User is now un-muted.");
					return true;
				}
				cs.sendMessage(ChatColor.RED + "Parameter not recognized as a command.");
				return false;
			}
			Channel c = ChannelCtrl.getChannel(args[0]);
			if (c == null) {
				cs.sendMessage(ChatColor.RED + "No such channel!");
				return true;
			}
			if (!Utils.hasPerm(cs, "dwdchat.channel." + c.getName())) {
				cs.sendMessage(ChatColor.RED + "You do not have permission to join this channel.");
				return true;
			}
			Channel curChan = ChannelCtrl.getPlayerChannel(p);
			if (curChan != null) curChan.removeMember(p);
			c.addMember(p);
			cs.sendMessage(ChatColor.BLUE + "Joined " + ChatColor.GRAY + c.getName() + ChatColor.BLUE + ".");
			return true;
		}
		return false;
	}
}
