package net.downwithdestruction.dwdchat.commands;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import net.downwithdestruction.dwdchat.DwDChat;
import net.downwithdestruction.dwdchat.channel.Channel;
import net.downwithdestruction.dwdchat.channel.ChannelCtrl;
import net.downwithdestruction.dwdchat.utils.Logger;
import net.downwithdestruction.dwdchat.utils.Utils;
import net.downwithdestruction.dwdcommands.DwDCommands;
import net.downwithdestruction.dwdcommands.utils.AFKUtils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdGlobal implements CommandExecutor {
	private final Logger logger = Logger.getInstance();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("global")) {
			if (!Utils.isAuthorized(cs, "dwdchat.global")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (DwDChat.getInstance().isVanished((Player) cs)) {
				cs.sendMessage(ChatColor.DARK_AQUA + "You cannot chat while vanished.");
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			if (DwDChat.getInstance().hasDwDCommands) {
				if (cs instanceof Player) {
					AFKUtils.setLastMove((Player) cs, (new Date()).getTime());
					if (AFKUtils.isAfk((Player) cs)) {
						AFKUtils.unsetAfk((Player) cs);
						DwDCommands.getInstance().getServer().broadcastMessage(ChatColor.GRAY + ((Player) cs).getDisplayName() + " is no longer AFK.");
					}
				}
			}
			Channel c = ChannelCtrl.getChannel("global");
			Player p = (Player) cs;
			String newMessage = Utils.colorize(c.getChatFormat());
			newMessage = Utils.sanitizeInput(newMessage, false);
			newMessage = Utils.replaceVars(newMessage, p);
			String originalMessage = Utils.getFinalArg(args, 0);
			originalMessage = Utils.sanitizeInput(originalMessage, false);
			if (!Utils.isAuthorized(p, "dwdchat.color") || !c.getColorAllowed())
				originalMessage = Utils.removeColorCodes(originalMessage);
			else if (Utils.isAuthorized(p, "dwdchat.color"))
				originalMessage = Utils.colorize(originalMessage);
			else originalMessage = Utils.removeColorCodes(originalMessage);
			newMessage = newMessage.replaceAll("(?i)\\{message\\}", originalMessage);
			if (Utils.decolorize(originalMessage).trim().equals("")) return true;
			for (Player pl : DwDChat.getInstance().getServer().getOnlinePlayers())
				pl.sendMessage(newMessage);
			Utils.sendToConsole(newMessage);
			File chatLog = new File(DwDChat.getInstance().getDataFolder() + File.separator + "chatlogs" + File.separator + c.getName() + ".log");
			try {
				if (!chatLog.exists()) {
					chatLog.getParentFile().mkdirs();
					chatLog.createNewFile();
				}
				BufferedWriter out = new BufferedWriter(new FileWriter(chatLog, true));
				SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
				out.write(dateFormatGmt.format(new Date()) + " " +newMessage);
				out.newLine();
				out.close();
			} catch (IOException ioe) {
				logger.config("Failed to save to chat log file " + chatLog.getAbsolutePath() + " : " + ioe.getMessage());
			}
			return true;
		}
		return false;
	}
}
