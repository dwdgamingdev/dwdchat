package net.downwithdestruction.dwdchat.commands;

import net.downwithdestruction.dwdchat.DwDChat;
import net.downwithdestruction.dwdchat.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdDwDChat implements CommandExecutor {
	private final static DwDChat plugin = DwDChat.getInstance();
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("dwdchat")) {
			if (!Utils.isAuthorized(cs, "dwdchat.dwdchat")) {
				Utils.dispNoPerms(cs);
				return true;
			}
			if (args.length > 0) {
				if (args[0].equalsIgnoreCase("reload")) {
					plugin.reloadConfig();
					plugin.loadConfig();
					plugin.loadMutes();
					cs.sendMessage(ChatColor.BLUE + "DwDChat " + ChatColor.BLUE + " configuration reloaded.");
					return true;
				}
			}
			cs.sendMessage(ChatColor.BLUE + "DwDChat " + ChatColor.GRAY + "v" + plugin.getDescription().getVersion() + ChatColor.BLUE + ".");
			return true;
		}
		return false;
	}
}
