package net.downwithdestruction.dwdchat.utils;

import java.util.logging.Level;
import net.downwithdestruction.dwdchat.DwDChat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Logger {
	private static final Logger logger = new Logger();
	
	public static Logger getInstance() {
		return logger;
	}
	
	public void log (Level level, Object obj) {
		if (level == Level.INFO) {
			DwDChat.getInstance().getServer().getConsoleSender().sendMessage(
				ChatColor.AQUA + "[" + ChatColor.LIGHT_PURPLE + "DwDChat" + ChatColor.AQUA + "] " +
				ChatColor.RESET + obj
			);
		} else {
			Bukkit.getLogger().log(level, "[DwDChat] " + obj);
		}
	}
	
	public void info(Object obj) {
		log(Level.INFO, obj);
	}
	
	public void warn(Object obj) {
		log(Level.WARNING, obj);
	}
	
	public void severe(Object obj) {
		log(Level.SEVERE, obj);
	}
	
	public void config(Object obj) {
		log(Level.CONFIG, obj);
	}
}
