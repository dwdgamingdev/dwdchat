package net.downwithdestruction.dwdchat.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.downwithdestruction.dwdchat.DwDChat;
import org.apache.commons.lang.text.StrBuilder;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.RemoteConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class Utils {
	private final static DwDChat plugin = DwDChat.getInstance();
	private final static Logger logger = Logger.getInstance();
	
	public static boolean isAuthorized(final CommandSender cs, String node) {
		return  cs instanceof RemoteConsoleCommandSender ||
				cs instanceof ConsoleCommandSender ||
				cs.hasPermission("dwdchat.op") ||
				cs.hasPermission(node);
	}
	
	public static boolean hasPerm(final CommandSender cs, String node) {
		return  cs.hasPermission(node); // Even ops wont get approved for this unless they have the perm node set (* nodes will override still)
	}
	
	public static void dispNoPerms(CommandSender cs) {
		cs.sendMessage(ChatColor.RED + "You don't have permission for that!");
		logger.info(ChatColor.RED + cs.getName() + " was denied access to that!");
	}
	
	public static void sendToConsole(String str) {
		plugin.getServer().getConsoleSender().sendMessage(str);
	}
	
	public static String getTag(Player player, String type) {
		String str = "";
		ConfigurationSection nodes = plugin.getConfig().getConfigurationSection("dwdchat." + type);
		if (nodes == null) return "";
		if (player.isOp()) {
			if (type.equals("prefix")) {
				str = str + (String) plugin.getConfig().get("dwdchat.prefix.operator");
			}
			
			return colorize(str);
		}
		for (String node : nodes.getKeys(false)) {
			if (player.hasPermission("dwdchat." + type + "." + node)) {
				str = str + plugin.getConfig().get("dwdchat." + type + "." + node);
			}
		}
		return colorize(str);
	}
	
	public static String colorize(String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}
	
	public static String decolorize(String str) {
		return str.replaceAll("(?i)\u00a7[a-f0-9k-or]", "");
	}
	
	public static String removeColorCodes(String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "");
	}
	
	public static String removeCaps(String str, Integer range) {
		if (range < 1)
			return str;
		Pattern pattern = Pattern.compile("([A-Z]{" + range + ",300})");
		Matcher matcher = pattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find())
			matcher.appendReplacement(sb, Matcher.quoteReplacement(matcher.group().toLowerCase()));
		matcher.appendTail(sb);
		str = sb.toString();
		return str;
	}
	
	public static String join(String[] array, int position) {
		StrBuilder sb = new StrBuilder();
		for (int i = position; i < array.length; i++) {
			sb.append(array[i]);
			sb.append(" ");
		}
		return sb.substring(0, sb.length() - 1);
	}
	
	public static String sanitizeInput(String s, boolean vanilla) {
		s = s.replace("\\", "\\\\");
		if (vanilla) s = s.replace("%", "%%");
		s = s.replace("$", "\\$");
		return s;
	}
	
	public static String formatText(String originalMessage, CommandSender sender, String format) {
		if (originalMessage == null) originalMessage = "";
		if (sender == null) throw new NullPointerException("sender can't be null!");
		originalMessage = Utils.sanitizeInput(originalMessage, false);
		String newMessage = Utils.colorize(format);
		newMessage = Utils.sanitizeInput(newMessage, false);
		newMessage = Utils.colorize(newMessage);
		newMessage = newMessage.replaceAll("(?i)\\{name\\}", sender.getName());
		newMessage = newMessage.replaceAll("(?i)\\{dispname\\}", ((sender instanceof Player) ? ((Player) sender).getDisplayName() : sender.getName()));
		originalMessage = (Utils.isAuthorized(sender, "dwdchat.color")) ? Utils.colorize(originalMessage) : Utils.removeColorCodes(originalMessage);
		if (DwDChat.removeAllCaps && !Utils.isAuthorized(sender, "dwdchat.caps"))
			originalMessage = Utils.removeCaps(originalMessage, DwDChat.capsRange);
		newMessage = newMessage.replaceAll("(?i)\\{message\\}", originalMessage);
		if (sender instanceof Player) {
			String world = (sender instanceof Player) ? ((Player) sender).getWorld().getName() : "";
			newMessage = newMessage.replaceAll("(?i)\\{world\\}", world);
			newMessage = newMessage.replaceAll("(?i)\\{prefix\\}", Utils.getTag((Player) sender, "prefix"));
			newMessage = newMessage.replaceAll("(?i)\\{suffix\\}", Utils.getTag((Player) sender, "suffix"));
			newMessage = newMessage.replaceAll("(?i)\\{custom\\}", Utils.getTag((Player) sender, "custom"));
			newMessage = newMessage.replaceAll("(?i)\\{customTwo\\}", Utils.getTag((Player) sender, "customTwo"));
		}
		return newMessage;
	}
	
	public static String replaceVars(String newMessage, Player p) {
		newMessage = Utils.colorize(newMessage);
		newMessage = newMessage.replaceAll("(?i)\\{name\\}", p.getName());
		newMessage = newMessage.replaceAll("(?i)\\{dispname\\}", (p.getDisplayName() == null) ? p.getName() : p.getDisplayName());
		newMessage = newMessage.replaceAll("(?i)\\{prefix\\}", Utils.getTag(p, "prefix"));
		newMessage = newMessage.replaceAll("(?i)\\{suffix\\}", Utils.getTag(p, "suffix"));
		newMessage = newMessage.replaceAll("(?i)\\{custom\\}", Utils.getTag(p, "custom"));
		newMessage = newMessage.replaceAll("(?i)\\{customTwo\\}", Utils.getTag(p, "customTwo"));
		newMessage = newMessage.replaceAll("(?i)\\{world\\}", p.getWorld().getName());
		return newMessage;
	}
	
	public static String getFinalArg(final String[] args, final int start) {
		 final StringBuilder bldr = new StringBuilder();
		 for (int i = start; i < args.length; i++) {
			 if (i != start) {
				 bldr.append(" ");
			 }
			 bldr.append(args[i]);
		 }
		 return bldr.toString();
	}
	
	public static String censorMsg(String message) {
		for(String badword : plugin.badwords) {
			message = message.replaceAll(badword, Matcher.quoteReplacement(plugin.badwordReplacer));
		}
		return message;
	}
}
