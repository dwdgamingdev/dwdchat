package net.downwithdestruction.dwdchat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.downwithdestruction.dwdchat.channel.ChannelCtrl;
import net.downwithdestruction.dwdchat.commands.*;
import net.downwithdestruction.dwdchat.listeners.PlayerListener;
import net.downwithdestruction.dwdchat.utils.Logger;
import net.downwithdestruction.dwdchat.utils.Utils;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.kitteh.vanish.VanishPlugin;

public class DwDChat extends JavaPlugin {
	public static DwDChat instance;
	private final Logger logger = Logger.getInstance();
	private final PluginManager pluginManager = Bukkit.getPluginManager();
	
	public static Permission permission = null;
	public static Chat chat = null;
	public static VanishPlugin vnp = null;
	
	public static String sayFormat;
	public static String meFormat;
	public static Boolean removeAllCaps;
	public static Integer capsRange;
	
	public Boolean hasDwDCommands = false;
	public FileConfiguration mutes;
	public File mutesFile;
	public String mutesFileString;
	public List<String> badwords;
	public String badwordReplacer;
	
	public DwDChat() {
		instance = this;
	}
	
	public static DwDChat getInstance() {
		return instance;
	}
	
	public void onEnable() {
		if (pluginManager.isPluginEnabled("DwDCommands")) {
			logger.info(ChatColor.AQUA + "Detected DwDCommands, enabling AFKWatcher support!");
			hasDwDCommands = true;
		}
		setupPermissions();
		setupChat();
		vnp = (VanishPlugin) getServer().getPluginManager().getPlugin("VanishNoPacket");
		loadConfig();
		loadMutes();
		pluginManager.registerEvents(new PlayerListener(this), this);
		getCommand("channel").setExecutor(new CmdChannel(this));
		getCommand("dwdchat").setExecutor(new CmdDwDChat());
		getCommand("global").setExecutor(new CmdGlobal());
		getCommand("local").setExecutor(new CmdLocal());
		getCommand("me").setExecutor(new CmdMe());
		getCommand("say").setExecutor(new CmdSay());
		logger.info(ChatColor.YELLOW + "v" + this.getDescription().getVersion());
	}
	
	public void onDisable() {
		logger.info(ChatColor.YELLOW + "Plugin Disabled.");
	}
	
	public void loadConfig() {
		final List<String> defaultBadwords = new ArrayList<String>();
		defaultBadwords.add("(?i)fuck");
		defaultBadwords.add("(?i)shit");
		defaultBadwords.add("(?i)ass");
		defaultBadwords.add("(?i)damn");
		defaultBadwords.add("(?i)bitch");
		FileConfiguration config = getConfig();
		if (!config.contains("say-format")) config.addDefault("say-format", "&d[Server] {message}");
		if (!config.contains("me-format")) config.addDefault("me-format", "&d * &b{dispname}&d {message}");
		if (!config.contains("remove-all-caps")) config.addDefault("remove-all-caps", true);
		if (!config.contains("caps-range")) config.addDefault("caps-range", 3);
		if (!config.contains("mutes-file")) config.addDefault("mutes-file", "mutes.yml");
		if (!config.contains("censored-words")) config.addDefault("censored-words", defaultBadwords);
		if (!config.contains("censored-replacer")) config.addDefault("censored-replacer", "$$$");
		if (!config.contains("channels.default.name")) config.addDefault("channels.default.name", "Local");
		if (!config.contains("channels.default.admin")) config.addDefault("channels.default.admin", "OP");
		if (!config.contains("channels.default.default")) config.addDefault("channels.default.default", true);
		if (!config.contains("channels.default.chat-format")) config.addDefault("channels.default.chat-format", "[{world}]{prefix}{dispname}{cusotmTwo}{suffix}&r: {custom}{message}");
		if (!config.contains("channels.default.chat-radius")) config.addDefault("channels.default.chat-radius", 40D);
		if (!config.contains("channels.default.always-seen")) config.addDefault("channels.default.always-seen", true);
		if (!config.contains("channels.default.color-allowed")) config.addDefault("channels.default.color-allowed", true);
		if (!config.contains("channels.global.name")) config.addDefault("channels.global.name", "Global");
		if (!config.contains("channels.global.admin")) config.addDefault("channels.global.admin", "OP");
		if (!config.contains("channels.global.default")) config.addDefault("channels.global.default", false);
		if (!config.contains("channels.global.chat-format")) config.addDefault("channels.global.chat-format", "[Global]{prefix}{dispname}{cusotmTwo}{suffix}&r: {custom}{message}");
		if (!config.contains("channels.global.chat-radius")) config.addDefault("channels.global.chat-radius", 0);
		if (!config.contains("channels.global.always-seen")) config.addDefault("channels.global.always-seen", true);
		if (!config.contains("channels.global.color-allowed")) config.addDefault("channels.global.color-allowed", true);
		if (!config.contains("dwdchat.prefix.mod")) config.addDefault("dwdchat.prefix.mod", "[Mod]");
		if (!config.contains("dwdchat.prefix.admin")) config.addDefault("dwdchat.prefix.admin", "[Admin]");
		if (!config.contains("dwdchat.suffix.tag1")) config.addDefault("dwdchat.suffix.tag1", "[Tag1]");
		if (!config.contains("dwdchat.suffix.tag2")) config.addDefault("dwdchat.suffix.tag2", "[Tag2]");
		if (!config.contains("dwdchat.custom.blue")) config.addDefault("dwdchat.custom.blue", "&1");
		if (!config.contains("dwdchat.custom.red")) config.addDefault("dwdchat.custom.red", "&4");
		if (!config.contains("dwdchat.custom.italic")) config.addDefault("dwdchat.custom.italic", "&o");
		if (!config.contains("dwdchat.customTwo.operator")) config.addDefault("dwdchat.customTwo.operator", "[OP]");
		if (!config.contains("dwdchat.customTwo.structural-engineer")) config.addDefault("dwdchat.customTwo.structural-engineer", "[SE]");
		config.options().copyDefaults(true);
		saveConfig();
		ChannelCtrl.reload();
		ChannelCtrl.reloadAllChannels();
		sayFormat = config.getString("say-format", "&d[Server] {message}");
		meFormat = config.getString("me-format", "&d * &b{dispname}&d {message}");
		removeAllCaps = config.getBoolean("remove-all-caps", true);
		capsRange = config.getInt("caps-range", 3);
		mutesFileString = config.getString("mutes-file", "mutes.yml");
		badwords = config.getStringList("censored-words");
		badwordReplacer = config.getString("censored-replacer", "$$$");
		logger.config("Loaded default configuration file");
	}
	
	public void loadMutes() {
		mutesFile = new File(getDataFolder() + File.separator + mutesFileString);
		try {
			if (!mutesFile.exists()) {
				mutesFile.getParentFile().mkdirs();
				mutesFile.createNewFile();
				mutes = YamlConfiguration.loadConfiguration(mutesFile);
				mutes.options().copyDefaults(true);
				mutes.save(mutesFile);
				logger.config("Created new default configuration file: " + mutesFile.getAbsolutePath());
			}
		} catch (IOException e) {
			logger.severe("Config file at " + mutesFile.getAbsolutePath() + " failed to load: " + e.getMessage());
		}
		mutes = YamlConfiguration.loadConfiguration(mutesFile);
		logger.config("Loaded mutes configuration file: " + mutesFile.getAbsolutePath());
	}
	
	public boolean isVanished(Player p) {
		if (vnp == null) {
			vnp = (VanishPlugin) Bukkit.getServer().getPluginManager().getPlugin("VanishNoPacket");
			return false;
		} else return vnp.getManager().isVanished(p.getName());
	}
	
	public boolean isVanished(Player p, CommandSender cs) {
		if (vnp == null) {
			vnp = (VanishPlugin) Bukkit.getServer().getPluginManager().getPlugin("VanishNoPacket");
			return false;
		}
		return !Utils.isAuthorized(cs, "dwdcommands.seehidden") && vnp.getManager().isVanished(p);
	}
	
	private Boolean setupPermissions() {
		RegisteredServiceProvider<Permission> permissionProvider = this.getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
		if (permissionProvider != null) permission = permissionProvider.getProvider();
		return (permission != null);
	}
	
	private Boolean setupChat() {
		RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
		if (chatProvider != null) chat = chatProvider.getProvider();
		return (chat != null);
	}
}
