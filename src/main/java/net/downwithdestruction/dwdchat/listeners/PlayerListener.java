package net.downwithdestruction.dwdchat.listeners;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import net.downwithdestruction.dwdchat.DwDChat;
import net.downwithdestruction.dwdchat.channel.Channel;
import net.downwithdestruction.dwdchat.channel.ChannelCtrl;
import net.downwithdestruction.dwdchat.utils.Logger;
import net.downwithdestruction.dwdchat.utils.Utils;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {
	private final static Logger logger = Logger.getInstance();
	private DwDChat plugin;
	
	public PlayerListener(DwDChat plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void playerChat(AsyncPlayerChatEvent e) {
		if (e.isCancelled()) return;
		Player p = e.getPlayer();
		if (p == null) return;
		Channel c = ChannelCtrl.getPlayerChannel(p);
		if (c == null) return;
		final Long mutedAt = plugin.mutes.getLong(p.getName() + "." + c.getName() + ".muted", 0);
		if (mutedAt > 0) {
			final Long length = plugin.mutes.getLong(p.getName() + "." + c.getName() + ".length");
			final Long curTime = new Date().getTime();
			if ((mutedAt + (length / 1000) > curTime) || (length == 0)) {
				p.sendMessage(ChatColor.GRAY + "You are muted from this channel!");
				Utils.sendToConsole(ChatColor.LIGHT_PURPLE + "User " + p.getName() + " tried to talk in channel " + c.getName() + " but was denied for being muted.");
				Utils.sendToConsole(ChatColor.LIGHT_PURPLE + "Blocked message: " + e.getMessage());
				e.setCancelled(true);
				e.setMessage("");
				e.setFormat("");
				e.getRecipients().clear();
				return;
			} else {
				if (length != 0) {
					try {
						plugin.mutes.set(p.getName() + "." + c.getName(), null);
						plugin.mutes.save(plugin.mutesFile);
					} catch (IOException ioe) { /* UH OH */ }
				}
			}
		}
		String newMessage = Utils.colorize(c.getChatFormat());
		newMessage = Utils.sanitizeInput(newMessage, false);
		newMessage = Utils.replaceVars(newMessage, p);
		String originalMessage = Utils.censorMsg(e.getMessage());
		originalMessage = Utils.sanitizeInput(originalMessage, false);
		if (!Utils.isAuthorized(p, "dwdchat.color") || !c.getColorAllowed())
			originalMessage = Utils.removeColorCodes(originalMessage);
		else if (Utils.isAuthorized(p, "dwdchat.color"))
			originalMessage = Utils.colorize(originalMessage);
		else originalMessage = Utils.removeColorCodes(originalMessage);
		newMessage = newMessage.replaceAll("(?i)\\{message\\}", originalMessage);
		if (Utils.decolorize(originalMessage).trim().equals("")) {
			e.setCancelled(true);
			e.setFormat("");
			e.setMessage("");
			e.getRecipients().clear();
			return;
		}
		if (!c.getAlwaysSeen()) {
			if (c.getChatRadius() > 0D) {
				for (Player pl : c.getPlayers()) {
					if (!Utils.hasPerm(pl, "dwdchat.seeall")) {
						if (!pl.getWorld().equals(p.getWorld())) continue;
						double distance = p.getLocation().distance(pl.getLocation());
						if (distance > c.getChatRadius()) continue;
					}
					if (e.getRecipients().contains(pl)) {
						pl.sendMessage(newMessage);
					}
				}
			}
		} else {
			for (Player pl : DwDChat.getInstance().getServer().getOnlinePlayers()) {
				if (c.getChatRadius() > 0D) {
					if (!Utils.hasPerm(pl, "dwdchat.seeall")) {
						if (!pl.getWorld().equals(p.getWorld())) continue;
						double distance = p.getLocation().distance(pl.getLocation());
						if (distance > c.getChatRadius()) continue;
					}
				}
				if (e.getRecipients().contains(pl)) {
					pl.sendMessage(newMessage);
				}
			}
		}
		Utils.sendToConsole(newMessage);
		File chatLog = new File(DwDChat.getInstance().getDataFolder() + File.separator + "chatlogs" + File.separator + c.getName() + ".log");
		try {
			if (!chatLog.exists()) {
				chatLog.getParentFile().mkdirs();
				chatLog.createNewFile();
			}
			BufferedWriter out = new BufferedWriter(new FileWriter(chatLog, true));
			SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
			out.write(dateFormatGmt.format(new Date()) + " " +newMessage);
			out.newLine();
			out.close();
		} catch (IOException ioe) {
			logger.config("Failed to save to chat log file " + chatLog.getAbsolutePath() + " : " + ioe.getMessage());
		}
		// cancel the vanilla chat methods to prevent leakage.
		e.setCancelled(true);
		e.setMessage("");
		e.setFormat("");
		e.getRecipients().clear();
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void playerJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		if (ChannelCtrl.getPlayerChannel(p) == null) {
			if (!ChannelCtrl.addToDefaultChannel(p)) {
				logger.info(ChatColor.RED + "There is no default channel set! Chat may look odd.");
			}
		}
	}
}
